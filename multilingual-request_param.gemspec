# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'multilingual-request_param/version'

Gem::Specification.new do |gem|
  gem.name          = "multilingual-request_param"
  gem.version       = Multilingual::RequestParam::VERSION
  gem.authors       = ["TAKAHASHI Kazunari"]
  gem.email         = ["takahashi@1syo.net"]
  gem.description   = %q{TODO: Write a gem description}
  gem.summary       = %q{TODO: Write a gem summary}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
end
